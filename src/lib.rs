
pub fn hello_world() -> &'static str {
    "Hello world"
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_hello_world() {
        assert_eq!(crate::hello_world(), "Hello world");
    }
}
